import com.company.dao.AirplaneDao;
import com.company.entity.Airplane;
import com.company.enums.DaoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class AirplaneRepositoryTest {

    @Test
    void shouldNotNull() {
        AirplaneDao dao = DaoFactory.INSTANCE.getAirplaneDao();
        Optional<List<Airplane>> airplanesOpt = dao.getAll();
        Assertions.assertTrue(airplanesOpt.isPresent());
    }
}
