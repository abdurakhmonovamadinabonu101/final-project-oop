import com.company.entity.auth.AuthRole;
import com.company.enums.DaoFactory;
import com.company.dao.auth.AuthRoleDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class AuthAuthRoleDaoTest {

    @Test
    void shouldNotNull() {
        AuthRoleDao dao = DaoFactory.INSTANCE.getAuthRoleDao();
        Optional<List<AuthRole>> rolesOpt = dao.getAll();
        Assertions.assertTrue(rolesOpt.isPresent());
    }

    @Test
    void shouldFindAll() {
        AuthRoleDao dao = DaoFactory.INSTANCE.getAuthRoleDao();

        List<AuthRole> expectedList = List.of(
                new AuthRole(1L, "Administrator", "ADMIN"),
                new AuthRole(2L, "AuthUser", "USER")
        );

        Assertions.assertArrayEquals(expectedList.toArray(), dao.getAll().get().toArray());
    }
}
