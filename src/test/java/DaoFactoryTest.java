import com.company.enums.DaoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DaoFactoryTest {

    @Test
    void shouldReturnRoleDao() {
        Assertions.assertNotNull(DaoFactory.INSTANCE.getAuthRoleDao());
    }

    @Test
    void shouldReturnUserDao() {
        Assertions.assertNotNull(DaoFactory.INSTANCE.getAuthUserDao());
    }

    @Test
    void shouldReturnCarDao() {
        Assertions.assertNotNull(DaoFactory.INSTANCE.getCarDao());
    }

    @Test
    void shouldReturnAirplaneDao() {
        Assertions.assertNotNull(DaoFactory.INSTANCE.getAirplaneDao());
    }

    @Test
    void shouldReturnDollDao() {
        Assertions.assertNotNull(DaoFactory.INSTANCE.getDollDao());
    }
}
