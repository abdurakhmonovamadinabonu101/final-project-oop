import com.company.entity.Car;
import com.company.enums.DaoFactory;
import com.company.dao.CarDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class CarRepositoryTest {

    @Test
    void shouldNotNull() {
        CarDao dao = DaoFactory.INSTANCE.getCarDao();
        Optional<List<Car>> carsOpt = dao.getAll();
        Assertions.assertTrue(carsOpt.isPresent());
    }
}
