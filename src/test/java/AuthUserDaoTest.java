import com.company.entity.auth.AuthUser;
import com.company.enums.DaoFactory;
import com.company.dao.auth.AuthUserDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class AuthUserDaoTest {

    @Test
    void shouldNotNull() {
        AuthUserDao dao = DaoFactory.INSTANCE.getAuthUserDao();
        Optional<List<AuthUser>> usersOpt = dao.getAll();
        Assertions.assertTrue(usersOpt.isPresent());
    }

    @Test
    void shouldDefaultAdmin() {
        AuthUserDao dao = DaoFactory.INSTANCE.getAuthUserDao();
        AuthUser admin = new AuthUser(
                1L, "admin", "123", "John", "Johnson", "21", "San-Fransisco"
        );
        Assertions.assertNotNull(dao.getAll().get().stream().filter(user -> user.equals(admin)));
    }
}
