import com.company.dao.CarDao;
import com.company.dao.DollDao;
import com.company.entity.Car;
import com.company.entity.Doll;
import com.company.enums.DaoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class DollRepositoryTest {

    @Test
    void shouldNotNull() {
        DollDao dao = DaoFactory.INSTANCE.getDollDao();
        Optional<List<Doll>> dollOpt = dao.getAll();
        Assertions.assertTrue(dollOpt.isPresent());
    }
}
