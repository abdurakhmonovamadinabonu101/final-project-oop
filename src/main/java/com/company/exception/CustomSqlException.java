package com.company.exception;

import lombok.Getter;

@Getter
public class CustomSqlException extends RuntimeException {
    public CustomSqlException(String message) {
        super(message);
    }
}
