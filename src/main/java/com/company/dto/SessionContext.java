package com.company.dto;

import com.company.entity.auth.AuthRole;
import com.company.entity.auth.AuthUser;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessionContext {
    private static SessionContext instance;
    private AuthUser sessionAuthUser;
    private AuthRole authRole;

    public static SessionContext getInstance() {
        if (instance == null) {
            instance = new SessionContext();
        }
        return instance;
    }
}
