package com.company.dao;

import com.company.entity.AuditEntity;

public interface CDDao<T extends AuditEntity> {
    void create(T t);

    void delete(Long id);
}
