package com.company.dao;

import com.company.entity.Airplane;
import com.company.exception.CustomRuntimeException;
import com.company.exception.CustomSqlException;
import com.company.util.CustomFileHandler;
import com.company.util.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AirplaneDao extends AbstractDao<Airplane> implements CDDao<Airplane>, ShowDao<Airplane> {

    protected static String path = "src/main/resources/airplanes.csv";

    @Override
    public void create(Airplane airplane) {
        try {
            CustomFileHandler.writeToFile(path, airplane.toString());
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        try {
            CustomFileHandler.delete(path, id, "id,color,price,ageRange,quantity,creationDate");
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public Optional<List<Airplane>> getAll() {
        try {
            return Optional.of(CustomMapper.toAirplanes(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new CustomRuntimeException(e.getMessage());
        }
    }
}
