package com.company.dao;

import com.company.entity.Doll;
import com.company.exception.CustomRuntimeException;
import com.company.exception.CustomSqlException;
import com.company.util.CustomFileHandler;
import com.company.util.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class DollDao extends AbstractDao<Doll> implements CDDao<Doll>, ShowDao<Doll> {

    protected static String path = "src/main/resources/dolls.csv";

    @Override
    public void create(Doll doll) {
        try {
            CustomFileHandler.writeToFile(path, doll.toString());
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        try {
            CustomFileHandler.delete(path, id, "id,color,price,ageRange,quantity,name,height,voice");
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public Optional<List<Doll>> getAll() {
        try {
            return Optional.of(CustomMapper.toDolls(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new CustomRuntimeException(e.getMessage());
        }
    }
}
