package com.company.dao;

import com.company.entity.Car;
import com.company.exception.CustomRuntimeException;
import com.company.exception.CustomSqlException;
import com.company.util.CustomFileHandler;
import com.company.util.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class CarDao extends AbstractDao<Car> implements CDDao<Car>, ShowDao<Car> {

    protected static String path = "src/main/resources/cars.csv";

    @Override
    public void create(Car car) {
        try {
            CustomFileHandler.writeToFile(path, car.toString());
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long id) {
        try {
            CustomFileHandler.delete(path, id, "id,color,price,ageRange,quantity,name,speed,weight");
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public Optional<List<Car>> getAll() {
        try {
            return Optional.of(CustomMapper.toCars(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new CustomRuntimeException(e.getMessage());
        }
    }
}
