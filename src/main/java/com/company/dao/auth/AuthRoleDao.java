package com.company.dao.auth;

import com.company.entity.auth.AuthRole;
import com.company.exception.CustomRuntimeException;
import com.company.dao.AbstractDao;
import com.company.dao.ShowDao;
import com.company.util.CustomFileHandler;
import com.company.util.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AuthRoleDao extends AbstractDao<AuthRole> implements ShowDao<AuthRole> {

    protected static String path = "src/main/resources/roles.csv";

    @Override
    public Optional<List<AuthRole>> getAll() {
        try {
            return Optional.of(CustomMapper.toRoles(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new CustomRuntimeException(e.getMessage());
        }
    }
}
