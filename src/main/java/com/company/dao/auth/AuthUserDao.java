package com.company.dao.auth;

import com.company.entity.auth.AuthUser;
import com.company.exception.CustomSqlException;
import com.company.dao.AbstractDao;
import com.company.dao.ShowDao;
import com.company.util.CustomFileHandler;
import com.company.util.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AuthUserDao extends AbstractDao<AuthUser> implements ShowDao<AuthUser> {

    protected static String path = "src/main/resources/users.csv";

    @Override
    public Optional<List<AuthUser>> getAll() {
        try {
            return Optional.of(CustomMapper.toUsers(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    public void create(AuthUser authUser) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append(authUser.getId()).append(",");
            stringBuilder.append(authUser.getUsername()).append(",");
            stringBuilder.append(authUser.getPassword()).append(",");
            stringBuilder.append(authUser.getFirstname()).append(",");
            stringBuilder.append(authUser.getLastname()).append(",");
            stringBuilder.append(authUser.getAge()).append(",");
            stringBuilder.append(authUser.getAddress());
            CustomFileHandler.writeToFile(path, stringBuilder.toString());
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }
}
