package com.company.dao.auth;

import com.company.entity.auth.AuthUserRole;
import com.company.exception.CustomRuntimeException;
import com.company.exception.CustomSqlException;
import com.company.dao.CDDao;
import com.company.dao.AbstractDao;
import com.company.dao.ShowDao;
import com.company.util.CustomFileHandler;
import com.company.util.CustomMapper;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class AuthUserRoleDao extends AbstractDao<AuthUserRole> implements ShowDao<AuthUserRole>, CDDao<AuthUserRole> {

    protected static String path = "src/main/resources/user_roles.csv";

    @Override
    public void create(AuthUserRole authUserRole) {
        Optional<List<AuthUserRole>> userRolesOpt = getAll();
        if (userRolesOpt.isEmpty())
            throw new CustomRuntimeException("AuthUser roles not found!");
        AuthUserRole newAuthUserRole = userRolesOpt.get().stream().filter(authUserRole1
                -> authUserRole1.getUserId().equals(authUserRole.getUserId())).findFirst().orElse(null);
        if (Objects.isNull(newAuthUserRole))
            attach(authUserRole);
        else
            updateUserRole(authUserRole);
    }

    private void updateUserRole(AuthUserRole authUserRole) {
        try {
            CustomFileHandler.delete(path, authUserRole.getUserId(), "userId,roleId");
            attach(authUserRole);
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    private void attach(AuthUserRole authUserRole) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(authUserRole.getUserId()).append(",");
        stringBuilder.append(authUserRole.getRoleId());
        try {
            CustomFileHandler.writeToFile(path, stringBuilder.toString());
        } catch (IOException e) {
            throw new CustomSqlException(e.getMessage());
        }
    }

    @Override
    public void delete(Long userRole) {
        throw new CustomRuntimeException("Method not supported!");
    }

    @Override
    public Optional<List<AuthUserRole>> getAll() {
        try {
            return Optional.of(CustomMapper.toUserRoles(CustomFileHandler.readFile(path)));
        } catch (IOException e) {
            throw new CustomRuntimeException(e.getMessage());
        }
    }

    public AuthUserRole getByUserId(Long userId) {
        return getAll().orElseThrow(()
                -> new CustomRuntimeException("AuthUser-roles not found!")).stream().filter(authUserRole
                -> authUserRole.getUserId().equals(userId)).findFirst().orElse(null);
    }
}
