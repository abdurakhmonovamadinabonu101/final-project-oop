package com.company.dao;

import com.company.entity.AuditEntity;

import java.util.List;
import java.util.Optional;

public interface ShowDao<T extends AuditEntity> {
    Optional<List<T>> getAll();
}
