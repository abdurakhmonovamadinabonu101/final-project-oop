package com.company.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Car extends BaseDomain {
    private String name;
    private Integer speed;
    private Double weight;

    public Car(Long id, String color, Double price, Integer ageRange, Integer quantity, String name, Integer speed, Double weight) {
        super(id, color, price, ageRange, quantity);
        this.name = name;
        this.speed = speed;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return id + "," + color + "," + price + "," + ageRange + ","
                + quantity + "," + name + "," + speed + "," + weight;
    }
}
