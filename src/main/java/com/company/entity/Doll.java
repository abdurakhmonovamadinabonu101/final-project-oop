package com.company.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Doll extends BaseDomain {
    private String name;
    private Double height;
    private boolean voice;

    public Doll(Long id, String color, Double price, Integer ageRange, Integer quantity, String name, Double height, boolean voice) {
        super(id, color, price, ageRange, quantity);
        this.name = name;
        this.height = height;
        this.voice = voice;
    }

    @Override
    public String toString() {
        return id + "," + color + "," + price + "," + ageRange + ","
                + quantity + "," + name + "," + height + "," + voice;
    }
}
