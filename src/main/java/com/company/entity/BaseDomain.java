package com.company.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseDomain extends AuditEntity {
    protected Long id;
    protected String color;
    protected Double price;
    protected Integer ageRange;
    protected Integer quantity;
}
