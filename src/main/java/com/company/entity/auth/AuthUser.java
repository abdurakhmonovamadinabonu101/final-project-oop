package com.company.entity.auth;

import com.company.entity.AuditEntity;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AuthUser extends AuditEntity {
    private Long id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String age;
    private String address;
}
