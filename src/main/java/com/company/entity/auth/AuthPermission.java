package com.company.entity.auth;

import com.company.entity.AuditEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthPermission extends AuditEntity {
    private Long id;
    private String name;
    private String code;
}
