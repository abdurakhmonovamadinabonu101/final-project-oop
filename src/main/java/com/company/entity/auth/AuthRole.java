package com.company.entity.auth;

import com.company.entity.AuditEntity;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class AuthRole extends AuditEntity {
    private Long id;
    private String name;
    private String code;
}
