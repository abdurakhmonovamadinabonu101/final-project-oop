package com.company.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Airplane extends BaseDomain {
    private String creationDate;

    public Airplane(Long id, String color, Double price, Integer ageRange, Integer quantity, String creationDate) {
        super(id, color, price, ageRange, quantity);
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return id + "," + color + "," + price + "," + ageRange + "," + quantity + "," + creationDate;
    }
}
