package com.company.service;

import com.company.entity.AuditEntity;

import java.util.List;
import java.util.Map;

public interface ShowService<T extends AuditEntity> {
    List<T> getAll();

    T get(Map<String, String> params);
}
