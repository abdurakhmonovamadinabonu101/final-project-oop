package com.company.service;

import com.company.dao.DollDao;
import com.company.entity.Doll;
import com.company.exception.CustomRuntimeException;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public class DollService implements CDService<Doll>, ShowService<Doll> {

    private DollDao dao;

    @Override
    public Long create(Map<String, String> params) {
        String color = params.get("color");
        if (color.isBlank())
            throw new CustomRuntimeException("Color can not be null!");

        String price = params.get("price");
        if (price.isBlank())
            throw new CustomRuntimeException("Price can not be null!");

        String ageRange = params.get("ageRange");
        if (ageRange.isBlank())
            throw new CustomRuntimeException("AgeRange can not be null!");

        String quantity = params.get("quantity");
        if (quantity.isBlank())
            throw new CustomRuntimeException("Quantity can not be null!");

        String name = params.get("name");
        if (name.isBlank())
            throw new CustomRuntimeException("Name can not be null!");

        String height = params.get("height");
        if (height.isBlank())
            throw new CustomRuntimeException("Height can not be null!");

        String voice = params.get("voice");
        if (voice.isBlank())
            throw new CustomRuntimeException("Voice can not be null!");

        long id;
        long max;
        List<Doll> dolls = getAll();
        if (dolls.isEmpty())
            id = 1;
        else {
            max = dolls.stream().max(Comparator.comparing(Doll::getId)).get().getId();
            id = max + 1;
        }
        dao.create(new Doll(id, color, Double.valueOf(price), Integer.valueOf(ageRange), Integer.valueOf(quantity),
                name, Double.valueOf(height), Boolean.parseBoolean(voice)));
        return id;
    }

    @Override
    public Boolean delete(Map<String, String> params) {
        dao.delete(get(params).getId());
        return true;
    }

    @Override
    public List<Doll> getAll() {
        Optional<List<Doll>> dollsOpt = dao.getAll();
        if (dollsOpt.isEmpty())
            throw new CustomRuntimeException("Doll not found!");
        return dollsOpt.get();
    }

    @Override
    public Doll get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new CustomRuntimeException("Id can not be null!");
        return getAll().stream().filter(doll -> doll.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new CustomRuntimeException("Doll not found with this id!"));
    }
}
