package com.company.service;

import com.company.dao.AirplaneDao;
import com.company.entity.Airplane;
import com.company.exception.CustomRuntimeException;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class AirplaneService implements CDService<Airplane>, ShowService<Airplane> {

    private AirplaneDao dao;

    @Override
    public Long create(Map<String, String> params) {
        String color = params.get("color");
        if (color.isBlank())
            throw new CustomRuntimeException("Color can not be null!");

        String price = params.get("price");
        if (price.isBlank())
            throw new CustomRuntimeException("Price can not be null!");

        String ageRange = params.get("ageRange");
        if (ageRange.isBlank())
            throw new CustomRuntimeException("AgeRange can not be null!");

        String quantity = params.get("quantity");
        if (quantity.isBlank())
            throw new CustomRuntimeException("Quantity can not be null!");

        String creationDate = params.get("creationDate");
        if (creationDate.isBlank())
            throw new CustomRuntimeException("CreationDate can not be null!");

        long id;
        long max;
        List<Airplane> airplanes = getAll();
        if (airplanes.isEmpty())
            id = 1;
        else {
            max = airplanes.stream().max(Comparator.comparing(Airplane::getId)).get().getId();
            id = max + 1;
        }
        dao.create(new Airplane(id, color, Double.valueOf(price), Integer.valueOf(ageRange), Integer.valueOf(quantity), creationDate));
        return id;
    }

    @Override
    public Boolean delete(Map<String, String> params) {
        dao.delete(get(params).getId());
        return true;
    }

    @Override
    public List<Airplane> getAll() {
        return dao.getAll().orElseThrow(()
                -> new CustomRuntimeException("Airplanes not found!"));
    }

    @Override
    public Airplane get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new CustomRuntimeException("Id cannot be null!");
        return dao.getAll().orElseThrow(() -> new CustomRuntimeException("Airplanes not found!"))
                .stream().filter(airplane -> airplane.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new CustomRuntimeException("Airplanes not found!"));
    }
}
