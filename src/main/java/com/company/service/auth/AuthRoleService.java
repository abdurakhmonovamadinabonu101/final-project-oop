package com.company.service.auth;

import com.company.entity.auth.AuthRole;
import com.company.entity.auth.AuthUserRole;
import com.company.exception.CustomRuntimeException;
import com.company.dao.auth.AuthRoleDao;
import com.company.dao.auth.AuthUserRoleDao;
import com.company.service.ShowService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@AllArgsConstructor
public class AuthRoleService implements ShowService<AuthRole> {

    private AuthRoleDao repository;
    private AuthUserRoleDao authUserRoleDao;
    private final AuthUserService authUserService;

    @Override
    public List<AuthRole> getAll() {
        Optional<List<AuthRole>> roleOpt = repository.getAll();
        if (roleOpt.isEmpty())
            throw new CustomRuntimeException("Roles not found!");
        return roleOpt.get();
    }

    @Override
    public AuthRole get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new CustomRuntimeException("Id can not be null!");
        return getAll().stream().filter(role -> role.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new CustomRuntimeException("AuthRole not found with this id!"));
    }

    public AuthRole getByUserId(Long userId) {
        AuthUserRole role = authUserRoleDao.getByUserId(userId);
        if (!Objects.isNull(role))
            return get(Map.of("id", role.getRoleId().toString()));
        return null;
    }

    public Boolean attachRole(Map<String, String> params) {
        String userId = params.get("userId");
        if (userId.isBlank())
            throw new CustomRuntimeException("AuthUser id can not be null!");
        if (Objects.isNull(authUserService.get(Map.of("id", userId))))
            throw new CustomRuntimeException("AuthUser not found with this id!");

        String roleId = params.get("roleId");
        if (roleId.isBlank())
            throw new CustomRuntimeException("AuthRole id can not be null!");
        if (Objects.isNull(get(Map.of("id", roleId))))
            throw new CustomRuntimeException("AuthRole not found with this id!");

        authUserRoleDao.create(new AuthUserRole(Long.valueOf(userId), Long.valueOf(roleId)));
        return true;
    }
}
