package com.company.service.auth;

import java.util.Map;

public interface AuthenticationService<T> {
    T login(Map<String, String> params);

    T register(Map<String, String> params);
}
