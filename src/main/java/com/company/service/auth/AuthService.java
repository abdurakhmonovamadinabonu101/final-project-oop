package com.company.service.auth;

import com.company.dto.SessionContext;
import com.company.entity.auth.AuthRole;
import com.company.entity.auth.AuthUser;
import com.company.exception.CustomRuntimeException;
import lombok.AllArgsConstructor;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
public class AuthService implements AuthenticationService<String> {

    private AuthUserService authUserService;
    private AuthRoleService authRoleService;

    @Override
    public String login(Map<String, String> params) {
        String username = params.get("username");
        String password = params.get("password");
        if (username.isBlank())
            throw new CustomRuntimeException("Username can not be null!");
        if (password.isBlank())
            throw new CustomRuntimeException("Password can not be null!");
        AuthUser authUser = authUserService.getByUsername(username);
        if (Objects.isNull(authUser))
            throw new CustomRuntimeException("AuthUser not found with this username!");
        if (!authUser.getPassword().equals(password))
            throw new CustomRuntimeException("Wrong password entered!");
        AuthRole authRole = authRoleService.getByUserId(authUser.getId());
        if (Objects.isNull(authRole))
            throw new CustomRuntimeException("AuthUser has no authRole, please contact the admin!");
        SessionContext instance = SessionContext.getInstance();
        instance.setSessionAuthUser(authUser);
        instance.setAuthRole(authRole);
        authRoleService.getByUserId(authUser.getId());
        return "You are successfully logged in!";
    }

    @Override
    public String register(Map<String, String> params) {
        String username = params.get("username");
        if (username.isBlank())
            throw new CustomRuntimeException("Username can not be null!");

        String password = params.get("password");
        if (password.isBlank())
            throw new CustomRuntimeException("Password can not be null!");

        String firstname = params.get("firstname");
        if (firstname.isBlank())
            throw new CustomRuntimeException("Firstname not be null!");

        String lastname = params.get("lastname");
        if (lastname.isBlank())
            throw new CustomRuntimeException("Lastname can not be null!");

        String age = params.get("age");
        if (age.isBlank())
            throw new CustomRuntimeException("Age can not be null!");

        String address = params.get("address");
        if (address.isBlank())
            throw new CustomRuntimeException("Address can not be null!");

        AuthUser authUserCheck = authUserService.getByUsername(username);
        if (!Objects.isNull(authUserCheck))
            throw new CustomRuntimeException("Username already taken!");

        long id;
        long max;
        List<AuthUser> authUsers = authUserService.getAll();
        if (authUsers.isEmpty())
            id = 1;
        else {
            max = authUsers.stream().max(Comparator.comparing(AuthUser::getId)).get().getId();
            id = max + 1;
        }
        AuthUser authUser = new AuthUser(id, username, password, firstname, lastname, age, address);
        authUserService.create(authUser);
        return "You are successfully registered!";
    }
}
