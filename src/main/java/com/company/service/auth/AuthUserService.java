package com.company.service.auth;

import com.company.entity.auth.AuthUser;
import com.company.exception.CustomRuntimeException;
import com.company.dao.auth.AuthUserDao;
import com.company.service.ShowService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public class AuthUserService implements ShowService<AuthUser> {

    private AuthUserDao repository;

    @Override
    public List<AuthUser> getAll() {
        Optional<List<AuthUser>> userOpt = repository.getAll();
        if (userOpt.isEmpty())
            throw new CustomRuntimeException("AuthUser not found!");
        return userOpt.get();
    }

    @Override
    public AuthUser get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new CustomRuntimeException("Id can not be null!");
        Optional<List<AuthUser>> userOpt = repository.getAll();
        if (userOpt.isEmpty())
            throw new CustomRuntimeException("AuthUser not found!");
        return userOpt.get().stream().filter(user -> user.getId().equals(Long.valueOf(id))).
                findFirst().orElseThrow(() -> new CustomRuntimeException("AuthUser not found with this id!"));
    }

    public AuthUser getByUsername(String username) {
        Optional<List<AuthUser>> userOpt = repository.getAll();
        if (userOpt.isEmpty())
            throw new CustomRuntimeException("AuthUser not found!");
        List<AuthUser> authUsers = userOpt.get();
        return authUsers.stream().filter(user -> user.getUsername().equals(username))
                .findFirst().orElse(null);
    }

    public void create(AuthUser authUser) {
        repository.create(authUser);
    }
}
