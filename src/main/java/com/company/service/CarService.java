package com.company.service;

import com.company.entity.Car;
import com.company.exception.CustomRuntimeException;
import com.company.dao.CarDao;
import lombok.AllArgsConstructor;

import java.util.*;

@AllArgsConstructor
public class CarService implements CDService<Car>, ShowService<Car> {

    private CarDao dao;

    @Override
    public Long create(Map<String, String> params) {
        String color = params.get("color");
        if (color.isBlank())
            throw new CustomRuntimeException("Color can not be null!");

        String price = params.get("price");
        if (price.isBlank())
            throw new CustomRuntimeException("Price can not be null!");

        String ageRange = params.get("ageRange");
        if (ageRange.isBlank())
            throw new CustomRuntimeException("AgeRange can not be null!");

        String quantity = params.get("quantity");
        if (quantity.isBlank())
            throw new CustomRuntimeException("Quantity can not be null!");

        String name = params.get("name");
        if (name.isBlank())
            throw new CustomRuntimeException("Name can not be null!");

        String speed = params.get("speed");
        if (speed.isBlank())
            throw new CustomRuntimeException("Speed can not be null!");

        String weight = params.get("weight");
        if (weight.isBlank())
            throw new CustomRuntimeException("Weight can not be null!");

        long id;
        long max;
        List<Car> cars = getAll();
        if (cars.isEmpty())
            id = 1;
        else {
            max = cars.stream().max(Comparator.comparing(Car::getId)).get().getId();
            id = max + 1;
        }
        dao.create(new Car(id, color, Double.valueOf(price), Integer.valueOf(ageRange), Integer.valueOf(quantity),
                name, Integer.valueOf(speed), Double.valueOf(weight)));
        return id;
    }

    @Override
    public Boolean delete(Map<String, String> params) {
        dao.delete(get(params).getId());
        return true;
    }

    @Override
    public List<Car> getAll() {
        Optional<List<Car>> carsOpt = dao.getAll();
        if (carsOpt.isEmpty())
            throw new CustomRuntimeException("Car not found!");
        return carsOpt.get();
    }

    @Override
    public Car get(Map<String, String> params) {
        String id = params.get("id");
        if (id.isBlank())
            throw new CustomRuntimeException("Id can not be null!");
        return getAll().stream().filter(car -> car.getId().equals(Long.valueOf(id)))
                .findFirst().orElseThrow(() -> new CustomRuntimeException("Car not found with this id!"));
    }
}
