package com.company.service;

import com.company.entity.AuditEntity;

import java.util.Map;

public interface CDService<T extends AuditEntity> {
    Long create(Map<String, String> params);

    Boolean delete(Map<String, String> params);
}
