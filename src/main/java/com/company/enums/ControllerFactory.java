package com.company.enums;

import com.company.controller.AirplaneController;
import com.company.controller.CarController;
import com.company.controller.DollController;
import com.company.controller.auth.AuthController;
import com.company.controller.auth.AuthRoleController;
import com.company.controller.auth.AuthUserController;

public enum ControllerFactory {
    INSTANCE;

    public CarController getCarController() {
        return new CarController(ServiceFactory.INSTANCE.getCarService());
    }

    public AirplaneController getAirplaneController() {
        return new AirplaneController(ServiceFactory.INSTANCE.getAirplaneService());
    }

    public AuthUserController getAuthUserController() {
        return new AuthUserController(ServiceFactory.INSTANCE.getAuthUserService());
    }

    public AuthRoleController getAuthRoleController() {
        return new AuthRoleController(ServiceFactory.INSTANCE.getAuthRoleService());
    }

    public AuthController getAuthController() {
        return new AuthController(ServiceFactory.INSTANCE.getAuthService());
    }

    public DollController getDollController() {
        return new DollController(ServiceFactory.INSTANCE.getDollService());
    }
}
