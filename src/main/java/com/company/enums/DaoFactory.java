package com.company.enums;

import com.company.dao.AirplaneDao;
import com.company.dao.CarDao;
import com.company.dao.DollDao;
import com.company.dao.auth.AuthRoleDao;
import com.company.dao.auth.AuthUserDao;
import com.company.dao.auth.AuthUserRoleDao;

public enum DaoFactory {
    INSTANCE;

    public CarDao getCarDao() {
        return new CarDao();
    }

    public AirplaneDao getAirplaneDao() {
        return new AirplaneDao();
    }

    public AuthUserDao getAuthUserDao() {
        return new AuthUserDao();
    }

    public AuthRoleDao getAuthRoleDao() {
        return new AuthRoleDao();
    }

    public AuthUserRoleDao getAuthUserRoleDao() {
        return new AuthUserRoleDao();
    }

    public DollDao getDollDao() {
        return new DollDao();
    }
}
