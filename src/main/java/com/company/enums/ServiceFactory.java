package com.company.enums;

import com.company.service.AirplaneService;
import com.company.service.CarService;
import com.company.service.DollService;
import com.company.service.auth.AuthRoleService;
import com.company.service.auth.AuthService;
import com.company.service.auth.AuthUserService;

public enum ServiceFactory {
    INSTANCE;

    public CarService getCarService() {
        return new CarService(DaoFactory.INSTANCE.getCarDao());
    }

    public AirplaneService getAirplaneService() {
        return new AirplaneService(DaoFactory.INSTANCE.getAirplaneDao());
    }

    public AuthUserService getAuthUserService() {
        return new AuthUserService(DaoFactory.INSTANCE.getAuthUserDao());
    }

    public AuthRoleService getAuthRoleService() {
        return new AuthRoleService(DaoFactory.INSTANCE.getAuthRoleDao(), DaoFactory.INSTANCE.getAuthUserRoleDao(), ServiceFactory.INSTANCE.getAuthUserService());
    }

    public AuthService getAuthService() {
        return new AuthService(getAuthUserService(), getAuthRoleService());
    }

    public DollService getDollService() {
        return new DollService(DaoFactory.INSTANCE.getDollDao());
    }
}
