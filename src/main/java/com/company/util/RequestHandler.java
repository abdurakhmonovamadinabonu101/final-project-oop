package com.company.util;

import com.company.controller.AirplaneController;
import com.company.controller.CarController;
import com.company.controller.DollController;
import com.company.controller.auth.AuthController;
import com.company.controller.auth.AuthRoleController;
import com.company.controller.auth.AuthUserController;
import com.company.dto.ResponseEntity;
import com.company.enums.ControllerFactory;
import com.company.exception.CustomRuntimeException;

import java.util.Map;

public class RequestHandler {
    private static final CarController CAR_CONTROLLER = ControllerFactory.INSTANCE.getCarController();
    private static final DollController DOLL_CONTROLLER = ControllerFactory.INSTANCE.getDollController();
    private static final AirplaneController AIRPLANE_CONTROLLER = ControllerFactory.INSTANCE.getAirplaneController();
    private static final AuthUserController AUTH_USER_CONTROLLER = ControllerFactory.INSTANCE.getAuthUserController();
    private static final AuthRoleController AUTH_ROLE_CONTROLLER = ControllerFactory.INSTANCE.getAuthRoleController();
    private static final AuthController authController = ControllerFactory.INSTANCE.getAuthController();

    public static void sendRequest(String path, Map<String, String> params) {
        try {
            if (path.startsWith("/auth")) {
                ResponseHandler.sendResponse(handleAuthMethod(path, params));
            } else if (path.startsWith("/airplane")) {
                ResponseHandler.sendResponse(handleAirplaneMethod(path, params));
            } else if (path.startsWith("/car")) {
                ResponseHandler.sendResponse(handleCarMethod(path, params));
            } else if (path.startsWith("/doll")) {
                ResponseHandler.sendResponse(handleDollMethod(path, params));
            } else if (path.startsWith("/Auth-user")) {
                ResponseHandler.sendResponse(handleUserMethod(path, params));
            } else if (path.startsWith("/Auth-role")) {
                ResponseHandler.sendResponse(handleRoleMethod(path, params));
            }
        } catch (Exception e) {
            ResponseHandler.sendError(e.getMessage());
        }
    }

    private static ResponseEntity handleDollMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return DOLL_CONTROLLER.getAll();
        } else if (methodPath.startsWith("get")) {
            return DOLL_CONTROLLER.get(params);
        } else if (methodPath.startsWith("create")) {
            return DOLL_CONTROLLER.create(params);
        } else if (methodPath.startsWith("delete")) {
            return DOLL_CONTROLLER.delete(params);
        }
        return new ResponseEntity<>("Method not supported", false);
    }

    private static ResponseEntity handleAuthMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("login")) {
            return authController.login(params);
        } else if (methodPath.startsWith("register")) {
            return authController.register(params);
        } else {
            throw new CustomRuntimeException("Method not supported!");
        }
    }

    private static ResponseEntity handleAirplaneMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return AIRPLANE_CONTROLLER.getAll();
        } else if (methodPath.startsWith("get")) {
            return AIRPLANE_CONTROLLER.get(params);
        } else if (methodPath.startsWith("create")) {
            return AIRPLANE_CONTROLLER.create(params);
        } else if (methodPath.startsWith("delete")) {
            return AIRPLANE_CONTROLLER.delete(params);
        }
        return new ResponseEntity<>("Method not supported", false);
    }

    private static ResponseEntity handleCarMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return CAR_CONTROLLER.getAll();
        } else if (methodPath.startsWith("get")) {
            return CAR_CONTROLLER.get(params);
        } else if (methodPath.startsWith("create")) {
            return CAR_CONTROLLER.create(params);
        } else if (methodPath.startsWith("delete")) {
            return CAR_CONTROLLER.delete(params);
        }
        return new ResponseEntity<>("Method not supported", false);
    }

    private static ResponseEntity handleUserMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return AUTH_USER_CONTROLLER.getAll();
        } else if (methodPath.startsWith("get")) {
            return AUTH_USER_CONTROLLER.get(params);
        }
        return new ResponseEntity<>("Method not supported", false);
    }

    private static ResponseEntity handleRoleMethod(String path, Map<String, String> params) {
        String methodPath = getMethodPath(path);
        if (methodPath.startsWith("list")) {
            return AUTH_ROLE_CONTROLLER.getAll();
        } else if (methodPath.startsWith("get")) {
            return AUTH_ROLE_CONTROLLER.get(params);
        } else if (methodPath.startsWith("attach-role")) {
            return AUTH_ROLE_CONTROLLER.attachRole(params);
        }
        return new ResponseEntity<>("Method not supported", false);
    }

    private static String getMethodPath(String path) {
        String[] split = path.split("/");
        if (split.length > 2)
            return split[2];
        return "";
    }
}
