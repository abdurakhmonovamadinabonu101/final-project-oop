package com.company.util;

import com.company.constants.Colors;
import com.company.dto.ResponseEntity;

public class ResponseHandler {

    public static void sendResponse(ResponseEntity responseEntity) {
        ConsoleHelper.println(ConsoleHelper.gson.toJson(responseEntity), Colors.GREEN);
    }

    public static void sendError(String message) {
        ConsoleHelper.printError(ConsoleHelper.gson.toJson(new ResponseEntity<>(message, false)));
    }
}
