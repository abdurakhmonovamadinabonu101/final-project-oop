package com.company.util;

import com.company.entity.Airplane;
import com.company.entity.Car;
import com.company.entity.Doll;
import com.company.entity.auth.AuthRole;
import com.company.entity.auth.AuthUser;
import com.company.entity.auth.AuthUserRole;

import java.util.List;
import java.util.stream.Collectors;

public class CustomMapper {

    public static List<AuthRole> toRoles(List<String> roles) {
        return roles.stream().map(CustomMapper::toRole).collect(Collectors.toList());
    }

    public static AuthRole toRole(String role) {
        String[] strings = role.split(",");
        return new AuthRole(Long.valueOf(strings[0]), strings[1], strings[2]);
    }

    public static List<AuthUser> toUsers(List<String> users) {
        return users.stream().map(CustomMapper::toUser).collect(Collectors.toList());
    }

    public static AuthUser toUser(String user) {
        String[] strings = user.split(",");
        return new AuthUser(Long.valueOf(strings[0]), strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]);
    }

    public static List<Car> toCars(List<String> tablewares) {
        return tablewares.stream().map(CustomMapper::toCar).collect(Collectors.toList());
    }

    public static Car toCar(String tableware) {
        String[] strings = tableware.split(",");
        return new Car(Long.valueOf(strings[0]), strings[1], Double.valueOf(strings[2]), Integer.valueOf(strings[3]),
                Integer.valueOf(strings[4]), strings[5], Integer.valueOf(strings[6]), Double.valueOf(strings[7]));
    }

    public static List<Airplane> toAirplanes(List<String> householdGoods) {
        return householdGoods.stream().map(CustomMapper::toAirplane).collect(Collectors.toList());
    }

    public static Airplane toAirplane(String householdGood) {
        String[] strings = householdGood.split(",");
        return new Airplane(Long.valueOf(strings[0]), strings[1], Double.valueOf(strings[2]), Integer.valueOf(strings[3]),
                Integer.valueOf(strings[4]), strings[5]);
    }

    public static List<AuthUserRole> toUserRoles(List<String> householdGoods) {
        return householdGoods.stream().map(CustomMapper::toUserRole).collect(Collectors.toList());
    }

    public static AuthUserRole toUserRole(String householdGood) {
        String[] strings = householdGood.split(",");
        return new AuthUserRole(Long.valueOf(strings[0]), Long.valueOf(strings[1]));
    }

    public static List<Doll> toDolls(List<String> dolls) {
        return dolls.stream().map(CustomMapper::toDoll).collect(Collectors.toList());
    }

    public static Doll toDoll(String householdGood) {
        String[] strings = householdGood.split(",");
        return new Doll(Long.valueOf(strings[0]), strings[1], Double.valueOf(strings[2]), Integer.valueOf(strings[3]),
                Integer.valueOf(strings[4]), strings[5], Double.valueOf(strings[6]), Boolean.parseBoolean(strings[7]));
    }
}
