package com.company.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

public class CustomFileHandler {

    public static List<String> readFile(String path) throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get(path));
        List<String> list = br.lines().filter(Objects::nonNull).skip(1).toList();
        br.close();
        return list;
    }

    public static void writeToFile(String path, String data) throws IOException {
        BufferedWriter wr = new BufferedWriter(new FileWriter(path, true));
        wr.newLine();
        wr.write(data);
        wr.close();
    }

    public static void delete(String path, Long id, String head) throws IOException {
        List<String> strings = readFile(path);
        BufferedWriter wr = new BufferedWriter(new FileWriter(path));
        wr.write(head);
        for (String string : strings) {
            if (!string.startsWith(String.valueOf(id))) {
                wr.newLine();
                wr.write(string);
            }
        }
        wr.close();
    }
}
