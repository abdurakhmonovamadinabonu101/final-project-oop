package com.company.controller;

import com.company.dto.ResponseEntity;
import com.company.entity.Airplane;
import com.company.service.AirplaneService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class AirplaneController implements ShowController<Airplane>, CDController<Airplane>, AbstractController {

    private AirplaneService service;

    @Override
    public ResponseEntity<List<Airplane>> getAll() {
        return new ResponseEntity<>(service.getAll(), true);
    }

    @Override
    public ResponseEntity<Airplane> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), true);
    }

    @Override
    public ResponseEntity<Long> create(Map<String, String> params) {
        return new ResponseEntity<>(service.create(params), true);
    }

    @Override
    public ResponseEntity<Boolean> delete(Map<String, String> params) {
        return new ResponseEntity<>(service.delete(params), true);
    }
}
