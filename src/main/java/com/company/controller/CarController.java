package com.company.controller;

import com.company.dto.ResponseEntity;
import com.company.entity.Car;
import com.company.service.CarService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class CarController implements ShowController<Car>, CDController<Car>, AbstractController {

    private CarService service;

    @Override
    public ResponseEntity<List<Car>> getAll() {
        return new ResponseEntity<>(service.getAll(), true);
    }

    @Override
    public ResponseEntity<Car> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), true);
    }

    @Override
    public ResponseEntity<Long> create(Map<String, String> params) {
        return new ResponseEntity<>(service.create(params), true);
    }

    @Override
    public ResponseEntity<Boolean> delete(Map<String, String> params) {
        return new ResponseEntity<>(service.delete(params), true);
    }
}
