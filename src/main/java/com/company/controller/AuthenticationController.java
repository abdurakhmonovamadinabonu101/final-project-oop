package com.company.controller;

import com.company.dto.ResponseEntity;

import java.util.Map;

public interface AuthenticationController<T> {
    ResponseEntity<T> login(Map<String, String> params);

    ResponseEntity<T> register(Map<String, String> params);
}
