package com.company.controller;

import com.company.dto.ResponseEntity;
import com.company.entity.AuditEntity;

import java.util.Map;

public interface CDController<T extends AuditEntity> {
    ResponseEntity<Long> create(Map<String, String> params);

    ResponseEntity<Boolean> delete(Map<String, String> params);
}
