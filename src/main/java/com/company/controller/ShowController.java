package com.company.controller;

import com.company.dto.ResponseEntity;
import com.company.entity.AuditEntity;

import java.util.List;
import java.util.Map;

public interface ShowController<T extends AuditEntity> {

    ResponseEntity<List<T>> getAll();

    ResponseEntity<T> get(Map<String, String> params);
}
