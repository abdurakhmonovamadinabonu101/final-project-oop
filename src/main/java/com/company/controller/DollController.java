package com.company.controller;

import com.company.dto.ResponseEntity;
import com.company.entity.Doll;
import com.company.service.DollService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class DollController implements ShowController<Doll>, CDController<Doll>, AbstractController {

    private DollService service;

    @Override
    public ResponseEntity<List<Doll>> getAll() {
        return new ResponseEntity<>(service.getAll(), true);
    }

    @Override
    public ResponseEntity<Doll> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), true);
    }

    @Override
    public ResponseEntity<Long> create(Map<String, String> params) {
        return new ResponseEntity<>(service.create(params), true);
    }

    @Override
    public ResponseEntity<Boolean> delete(Map<String, String> params) {
        return new ResponseEntity<>(service.delete(params), true);
    }
}
