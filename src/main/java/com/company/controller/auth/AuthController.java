package com.company.controller.auth;

import com.company.controller.AuthenticationController;
import com.company.controller.AbstractController;
import com.company.dto.ResponseEntity;
import com.company.service.auth.AuthService;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class AuthController implements AbstractController, AuthenticationController<String> {

    private AuthService service;

    @Override
    public ResponseEntity<String> login(Map<String, String> params) {
        return new ResponseEntity<>(service.login(params), true);
    }

    @Override
    public ResponseEntity<String> register(Map<String, String> params) {
        return new ResponseEntity<>(service.register(params), true);
    }
}
