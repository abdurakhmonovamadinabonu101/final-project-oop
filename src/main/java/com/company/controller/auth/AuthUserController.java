package com.company.controller.auth;

import com.company.controller.AbstractController;
import com.company.controller.ShowController;
import com.company.dto.ResponseEntity;
import com.company.entity.auth.AuthUser;
import com.company.service.auth.AuthUserService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class AuthUserController implements ShowController<AuthUser>, AbstractController {

    private AuthUserService service;

    @Override
    public ResponseEntity<List<AuthUser>> getAll() {
        return new ResponseEntity<>(service.getAll(), true);
    }

    @Override
    public ResponseEntity<AuthUser> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), true);
    }
}
