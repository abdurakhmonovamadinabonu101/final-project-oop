package com.company.controller.auth;

import com.company.controller.AbstractController;
import com.company.controller.ShowController;
import com.company.dto.ResponseEntity;
import com.company.entity.auth.AuthRole;
import com.company.service.auth.AuthRoleService;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class AuthRoleController implements ShowController<AuthRole>, AbstractController {

    private AuthRoleService service;

    @Override
    public ResponseEntity<List<AuthRole>> getAll() {
        return new ResponseEntity<>(service.getAll(), true);
    }

    @Override
    public ResponseEntity<AuthRole> get(Map<String, String> params) {
        return new ResponseEntity<>(service.get(params), true);
    }

    public ResponseEntity<Boolean> attachRole(Map<String, String> params) {
        return new ResponseEntity<>(service.attachRole(params), true);
    }
}
