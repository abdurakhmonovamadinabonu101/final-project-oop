package com.company.ui;

import com.company.constants.Colors;
import com.company.dto.SessionContext;
import com.company.util.ConsoleHelper;
import com.company.util.RequestHandler;

import java.util.HashMap;
import java.util.Map;

public class UserInterface {

    public static void run() {
        ConsoleHelper.println("\n\n");
        ConsoleHelper.println("1 -->> Login");
        ConsoleHelper.println("2 -->> Register");
        ConsoleHelper.println("0 -->> Exit");

        String operation = ConsoleHelper.readText("Enter operation number: ");

        switch (operation) {
            case "0" -> System.exit(0);
            case "1" -> login();
            case "2" -> register();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        run();
    }

    private static void register() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.println("\n\nRegister page: ");
        ConsoleHelper.print("1.Firstname: ");
        params.put("firstname", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Lastname: ");
        params.put("lastname", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Username: ");
        params.put("username", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Password: ");
        params.put("password", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Age: ");
        params.put("age", ConsoleHelper.readText().trim());

        ConsoleHelper.print("6.Address: ");
        params.put("address", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/auth/register", params);
    }

    private static void login() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.println("\n\nLogin page: ");
        ConsoleHelper.print("1.Username: ");
        params.put("username", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Password: ");
        params.put("password", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/auth/login", params);
        menu();
    }

    private static void menu() {
        SessionContext sessionContext = SessionContext.getInstance();
        if (sessionContext.getSessionAuthUser() != null) {
            ConsoleHelper.println("\n\nMenu:");
            ConsoleHelper.println("1 -->> Car");
            ConsoleHelper.println("2 -->> Airplane");
            ConsoleHelper.println("3 -->> Doll");
            if (sessionContext.getAuthRole().getCode().equals("ADMIN")) {
                ConsoleHelper.println("4 -->> Show all users");
                ConsoleHelper.println("5 -->> Show all roles");
                ConsoleHelper.println("6 -->> Attach role to user");
            }
            ConsoleHelper.println("7 -->> Profile");
            ConsoleHelper.println("0 -->> Logout");

            String operation = ConsoleHelper.readText("Enter operation number: ");
            switch (operation) {
                case "0" -> logout();
                case "1" -> carUI(sessionContext);
                case "2" -> airplaneUI(sessionContext);
                case "3" -> dollUI(sessionContext);
                case "4" -> showAllUsers();
                case "5" -> showAllRoles();
                case "6" -> attachRole();
                case "7" -> profile();
                default -> ConsoleHelper.printError("Invalid operation number: choose available options");
            }
            menu();
        } else {
            run();
        }
    }

    private static void dollUI(SessionContext sessionContext) {
        ConsoleHelper.println("\n\n1 -->> Show all");
        ConsoleHelper.println("2 -->> Show by id");
        if (sessionContext.getAuthRole().getCode().equals("ADMIN")) {
            ConsoleHelper.println("3 -->> Create");
            ConsoleHelper.println("4 -->> Delete");
        }
        ConsoleHelper.println("0 -->> Back");
        String operation = ConsoleHelper.readText("Enter operation number: ");
        switch (operation) {
            case "0" -> menu();
            case "1" -> showAllDoll();
            case "2" -> showDollById();
            case "3" -> createDoll();
            case "4" -> deleteDoll();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        dollUI(sessionContext);
    }

    private static void deleteDoll() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/doll/delete", params);
    }

    private static void createDoll() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.Color: ");
        params.put("color", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Price: ");
        params.put("price", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Age range: ");
        params.put("ageRange", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Quantity: ");
        params.put("quantity", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Name: ");
        params.put("name", ConsoleHelper.readText().trim());

        ConsoleHelper.print("6.Height: ");
        params.put("height", ConsoleHelper.readText().trim());

        ConsoleHelper.print("7.Voice (true or false): ");
        params.put("voice", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/doll/create", params);
    }

    private static void showDollById() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/doll/get", params);
    }

    private static void showAllDoll() {
        RequestHandler.sendRequest("/doll/list", null);
    }

    private static void showAllRoles() {
        RequestHandler.sendRequest("/Auth-role/list", null);
    }

    private static void attachRole() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.Auth-user id: ");
        params.put("userId", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Auth-role id: ");
        params.put("roleId", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/Auth-role/attach-role", params);
    }

    private static void showAllUsers() {
        RequestHandler.sendRequest("/Auth-user/list", null);
    }

    private static void airplaneUI(SessionContext sessionContext) {
        ConsoleHelper.println("\n\n1 -->> Show all");
        ConsoleHelper.println("2 -->> Show by id");
        if (sessionContext.getAuthRole().getCode().equals("ADMIN")) {
            ConsoleHelper.println("3 -->> Create");
            ConsoleHelper.println("4 -->> Delete");
        }
        ConsoleHelper.println("0 -->> Back");
        String operation = ConsoleHelper.readText("Enter operation number: ");
        switch (operation) {
            case "0" -> menu();
            case "1" -> showAllAirplane();
            case "2" -> showAirplaneById();
            case "3" -> createAirplane();
            case "4" -> deleteAirplane();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        airplaneUI(sessionContext);
    }

    private static void carUI(SessionContext sessionContext) {
        ConsoleHelper.println("\n\n1 -->> Show all");
        ConsoleHelper.println("2 -->> Show by id");
        if (sessionContext.getAuthRole().getCode().equals("ADMIN")) {
            ConsoleHelper.println("3 -->> Create");
            ConsoleHelper.println("4 -->> Delete");
        }
        ConsoleHelper.println("0 -->> Back");
        String operation = ConsoleHelper.readText("Enter operation number: ");
        switch (operation) {
            case "0" -> menu();
            case "1" -> showAllCar();
            case "2" -> showCarById();
            case "3" -> createCar();
            case "4" -> deleteCar();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        carUI(sessionContext);
    }

    private static void profile() {
        ConsoleHelper.println(ConsoleHelper.gson.toJson(SessionContext.getInstance()), Colors.YELLOW);
    }

    private static void deleteAirplane() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/airplane/delete", params);
    }

    private static void createAirplane() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.Color: ");
        params.put("color", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Price: ");
        params.put("price", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Age range: ");
        params.put("ageRange", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Quantity: ");
        params.put("quantity", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Creation Date: ");
        params.put("creationDate", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/airplane/create", params);
    }

    private static void showAirplaneById() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/airplane/get", params);
    }

    private static void showAllAirplane() {
        RequestHandler.sendRequest("/airplane/list", null);
    }

    private static void deleteCar() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/car/delete", params);
    }

    private static void createCar() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.print("\n1.Color: ");
        params.put("color", ConsoleHelper.readText().trim());

        ConsoleHelper.print("2.Price: ");
        params.put("price", ConsoleHelper.readText().trim());

        ConsoleHelper.print("3.Age range: ");
        params.put("ageRange", ConsoleHelper.readText().trim());

        ConsoleHelper.print("4.Quantity: ");
        params.put("quantity", ConsoleHelper.readText().trim());

        ConsoleHelper.print("5.Name: ");
        params.put("name", ConsoleHelper.readText().trim());

        ConsoleHelper.print("6.Speed: ");
        params.put("speed", ConsoleHelper.readText().trim());

        ConsoleHelper.print("7.Weight: ");
        params.put("weight", ConsoleHelper.readText().trim());

        RequestHandler.sendRequest("/car/create", params);
    }

    private static void showCarById() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.print("\nEnter id: ");
        params.put("id", ConsoleHelper.readText().trim());
        RequestHandler.sendRequest("/car/get", params);
    }

    private static void showAllCar() {
        RequestHandler.sendRequest("/car/list", null);
    }

    private static void logout() {
        SessionContext instance = SessionContext.getInstance();
        instance.setSessionAuthUser(null);
        instance.setAuthRole(null);
    }
}
