package com.company;

import com.company.constants.AppConstants;
import com.company.ui.UserInterface;
import com.company.util.ConsoleHelper;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        ConsoleHelper.println(AppConstants.APP_NAME + " version: " + AppConstants.VERSION + "  " + new Date());
        ConsoleHelper.println("Developer name: " + AppConstants.DEV_NAME);
        ConsoleHelper.println("Developer email: " + AppConstants.DEV_EMAIL);
        UserInterface.run();
    }
}